"""
__Seed builder__v1.0
  (Read_only) Modify via SeedManifest.yaml
"""

from rest_framework import serializers
from serializers.helpers.serializer import Serializer
from models.developer import Developer
from serializers.user import UserSerializer

class _DeveloperSerializer(Serializer):  #
    
    
    
    class Meta:
        model = Developer
        fields = (
            'id',
            'hash',
            'description',
            'url_logo',
            'name_d',
        )
        depth = 1

