"""
__Seed builder__v1.0
  (Read_only) Modify via SeedManifest.yaml
"""

from rest_framework import serializers
from serializers.helpers.serializer import Serializer
from models.publisher import Publisher
from serializers.user import UserSerializer

class _PublisherSerializer(Serializer):  #
    
    
    
    class Meta:
        model = Publisher
        fields = (
            'id',
            'hash',
            'name_p',
            'description',
            'url_logo',
        )
        depth = 1
