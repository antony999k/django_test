"""
__Seed builder__v1.0
  (Read_only) Modify via SeedManifest.yaml
"""

from rest_framework import serializers
from serializers.helpers.serializer import Serializer
from models.game import Game
from models.publisher import Publisher
from serializers.user import UserSerializer

class _GameSerializer(Serializer):  #
    
    class PublisherSerializer(Serializer):
        class Meta(Serializer.Meta):
            model = Publisher
    
    publisher = PublisherSerializer(read_only=True)
    
    publisher_id = serializers.PrimaryKeyRelatedField(source='publisher', queryset=Publisher.objects.all())
    
    class Meta:
        model = Game
        fields = (
            'id',
            'hash',
            'name_g',
            'gender',
            'relase_date',
            'url_image',
            'publisher',
            'publisher_id',
        )
        depth = 1

