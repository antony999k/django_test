"""
__Seed builder__v1.0
  (Read_only) Modify via SeedManifest.yaml
"""

from views.helpers.viewsets import FullViewSet

from models.publisher import Publisher
from serializers.publisher import PublisherSerializer

class _PublisherViewSet(FullViewSet):
    serializer_class = PublisherSerializer
    queryset = Publisher.objects.all()
