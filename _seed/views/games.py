"""
__Seed builder__v1.0
  (Read_only) Modify via SeedManifest.yaml
"""

from views.helpers.viewsets import FullViewSet

from models.game import Game
from serializers.game import GameSerializer

class _GameViewSet(FullViewSet):
    serializer_class = GameSerializer
    queryset = Game.objects.all()
