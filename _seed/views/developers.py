"""
__Seed builder__v1.0
  (Read_only) Modify via SeedManifest.yaml
"""

from views.helpers.viewsets import FullViewSet

from models.developer import Developer
from serializers.developer import DeveloperSerializer

class _DeveloperViewSet(FullViewSet):
    serializer_class = DeveloperSerializer
    queryset = Developer.objects.all()
