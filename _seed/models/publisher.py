"""
__Seed builder__v1.0
  (Read_only) Modify via SeedManifest.yaml
"""

from datetime import datetime
from django.db import models
from models.helpers.model import Model

class _Publisher(Model):  #
    
    name_p = models.CharField(max_length=256)
    description = models.TextField(default="No description available")
    url_logo = models.CharField(max_length=256)

    class Meta:
        abstract = True

