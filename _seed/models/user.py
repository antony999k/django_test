"""
__Seed builder__v1.0
  (Read_only) Modify via SeedManifest.yaml
"""

from datetime import datetime
from django.contrib.auth.models import AbstractUser
from django.db import models
from models.helpers.model import Model

class _User(AbstractUser, Model):  #
    
    url_profile_pic = models.CharField(max_length=256)

    class Meta:
        abstract = True


