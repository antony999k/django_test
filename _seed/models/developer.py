"""
__Seed builder__v1.0
  (Read_only) Modify via SeedManifest.yaml
"""

from datetime import datetime
from django.db import models
from models.helpers.model import Model

class _Developer(Model):  #
    
    description = models.TextField(default="No description available")
    url_logo = models.CharField(max_length=256)
    name_d = models.CharField(max_length=256)

    class Meta:
        abstract = True


