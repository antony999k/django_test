"""
__Seed builder__v1.0
  (Read_only) Modify via SeedManifest.yaml
"""

from datetime import datetime
from django.db import models
from models.helpers.model import Model

class _Game(Model):  #
    
    GENDERS = (
        ('PLATFORM', 'Platform'),
        ('SHOOTER', 'Shooter'),
        ('FIGHTING', 'Fighting'),
        ('SURVIVAL', 'Survival'),
        ('HORROR', 'Horror'),
        ('RHYTHM', 'Rhythm'),
        ('METROIDVANIA', 'Metroidvania'),
        ('RPG', 'RPG'),
        ('MMORPG', 'MMORPG'),
        ('SIMULATION', 'Simulation'),
        ('STRATEGY', 'Strategy'),
        ('RACING', 'Racing'),
        ('SPORTS', 'Sports'),
        ('MMO', 'MMO'),
        ('PARTY', 'Party'),
        ('EDUCATIONAL', 'Educational'),
        ('OTHER', 'Other'),
    )
    name_g = models.CharField(max_length=256)
    gender = models.CharField(max_length=32, choices=GENDERS)
    relase_date = models.DateTimeField(default=datetime.now)
    url_image = models.CharField(max_length=256)
    publisher = models.ForeignKey('Publisher', related_name='games', 
        blank=False, null=False, on_delete=models.CASCADE)

    class Meta:
        abstract = True


