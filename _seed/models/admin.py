"""
__Seed builder__v1.0
  (Read_only) Modify via SeedManifest.yaml
"""

from django.contrib import admin
from models.user import User
from models.game import Game
from models.developer import Developer
from models.publisher import Publisher

class _Admin:  #

	@staticmethod
	def register():
		admin.site.register(User)
		admin.site.register(Game)
		admin.site.register(Developer)
		admin.site.register(Publisher)
