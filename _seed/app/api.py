"""
__Seed builder__v1.0
  (Read_only) Modify via SeedManifest.yaml
"""

from django.urls import path, include
from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from views.users import UserViewSet
from views.games import GameViewSet
from views.developers import DeveloperViewSet
from views.publishers import PublisherViewSet

router = DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'games', GameViewSet)
router.register(r'developers', DeveloperViewSet)
router.register(r'publishers', PublisherViewSet)

urlpatterns = [
    path('', include(router.urls)),
    url(r'^auth/', include('rest_auth.urls'))
]