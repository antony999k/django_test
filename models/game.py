"""
__Seed builder__v1.0

  Guidelines: 
    - Modify attributes via SeedManifest.yaml
    - Only add aggregate methods if required
      - Example: has_members(), is_frequent_user() ...

  Attributes:
    - id: int
    - name_g: string
    - gender: enum
    - relase_date: date
    - url_image: string
    - publisher: Publisher
"""

from django.db import models
from _seed.models.game import _Game

class Game(_Game):
    pass
