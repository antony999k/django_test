"""
__Seed builder__v1.0

  Guidelines: 
    - Modify attributes via SeedManifest.yaml
    - Only add aggregate methods if required
      - Example: has_members(), is_frequent_user() ...

  Attributes:
    - id: int
    - description: text
    - url_logo: string
    - name_d: string
"""

from django.db import models
from _seed.models.developer import _Developer

class Developer(_Developer):
    pass
