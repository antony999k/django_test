"""
__Seed builder__v1.0

  Guidelines: 
    - Modify attributes via SeedManifest.yaml
    - Only add aggregate methods if required
      - Example: has_members(), is_frequent_user() ...

  Attributes:
    - id: int
    - name_p: string
    - description: text
    - url_logo: string
"""

from django.db import models
from _seed.models.publisher import _Publisher

class Publisher(_Publisher):
    pass
