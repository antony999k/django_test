"""
__Seed builder__v1.0

  Guidelines: 
    - Modify fields via SeedManifest.yaml (suggested meta: "write & read")
    - Only override serializers if required
      - Example: When it's necessary to increase or limit sent data

  Fields:
    - id
    - name_g
    - gender
    - relase_date
    - url_image
    - publisher
    - publisher_id
    
  Serializers (to override)
    - PublisherSerializer
"""

from rest_framework import serializers
from _seed.serializers.game import _GameSerializer

class GameSerializer(_GameSerializer): #
    
    class Meta(_GameSerializer.Meta):
        pass
