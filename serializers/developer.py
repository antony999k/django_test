"""
__Seed builder__v1.0

  Guidelines: 
    - Modify fields via SeedManifest.yaml (suggested meta: "write & read")
    - Only override serializers if required
      - Example: When it's necessary to increase or limit sent data

  Fields:
    - id
    - description
    - url_logo
    - name_d
    
  Serializers (to override)
"""

from rest_framework import serializers
from _seed.serializers.developer import _DeveloperSerializer

class DeveloperSerializer(_DeveloperSerializer): #
    
    class Meta(_DeveloperSerializer.Meta):
        pass
