"""
__Seed builder__v1.0

  Guidelines: 
    - Modify fields via SeedManifest.yaml (suggested meta: "write & read")
    - Only override serializers if required
      - Example: When it's necessary to increase or limit sent data

  Fields:
    - id
    - name_p
    - description
    - url_logo
    
  Serializers (to override)
"""

from rest_framework import serializers
from _seed.serializers.publisher import _PublisherSerializer

class PublisherSerializer(_PublisherSerializer): #
    
    class Meta(_PublisherSerializer.Meta):
        pass